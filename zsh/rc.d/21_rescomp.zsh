### Aliases for rescomp machines

if [[ "$HOSTNAME" == *rescomp.berkeley.edu || "$HOSTNAME" == *housing.berkeley.edu ]] ; then
    ### Paths
    export SVNCODE="https://svn.rescomp.berkeley.edu/code"
    export SVNTMPL="https://svn.rescomp.berkeley.edu/marketing"
    export CODE="/usr/code/$USER/"

    ### PostgreSQL Database Access
    alias devdb='psql -h code -p 5432 rescomp'
    #For dev-cc, ssh into hal then dev-cougar first.
    #alias devcc='psql -h dev-sal -p 5432 cc'
    alias testdb='psql -h test-db -p 5432 rescomp'
    alias proddb='psql -h db rescomp'

    ### Dev util
    alias apacherl='sudo /usr/local/etc/rc.d/apache23 restart;sleep 5;sudo /usr/local/etc/rc.d/apache22 status'

    ### Log Access
    alias deverror='sudo /usr/bin/tail -f /var/log/httpd-error.log'
    alias devaccess='sudo /usr/bin/tail -f /var/log/httpd-access.log'
    #alias fixlogs='sudo /usr/local/etc/rc.d/syslog-ng restart'

    ### Webtree sync
    alias websync='sudo svn export --force $SVNTMPL/webtree/ /usr/local/www/rescomp/docs/'

    ### virtualenv
    # Adding credit in case I get yelled at
    # http://mrcoles.com/tips-using-pip-virtualenv-virtualenvwrapper/
    export WORKON_HOME="$CODE/virtualenvs"
    export VIRTUALENVWRAPPER_LOG_DIR="$WORKON_HOME"
    export VIRTUALENVWRAPPER_HOOK_DIR="$WORKON_HOME"
    source /usr/bin/virtualenvwrapper.sh
    alias v='workon'
    alias v.deactivate='deactivate'
    alias v.mk='mkvirtualenv --no-site-packages'
    alias v.mk_withsitepackages='mkvirtualenv'
    alias v.rm='rmvirtualenv'
    alias v.switch='workon'
    alias v.add2virtualenv='add2virtualenv'
    alias v.cdsitepackages='cdsitepackages'
    alias v.cd='cdvirtualenv'
    alias v.lssitepackages='lssitepackages'
fi
