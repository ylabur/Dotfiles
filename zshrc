#!/usr/bin/env zsh
####################################################
#  Jake Teton-Landis's                       ZSHRC #
# <just.1.jake@gmail.com>              Winter 2012 #
# Stolen and changed by Yuta Labur
# <https://www.github.com/ylabur>
####################################################

setopt ALL_EXPORT                                                                # export all of these settings
TZ="America/Los_Angeles"
HOSTNAME="`hostname`"
PAGER='less'                                                                     # less is more
EDITOR='vim'
[ -z "$SVN_EDITOR" ] && SVN_EDITOR="$EDITOR"                                     # set SVN_EDITOR only if unset
VISUAL="$EDITOR"
LC_ALL='en_US.UTF-8'                                                             #LANGUAGE=
DOTFILES="$HOME/.dotfiles"
ZSH_FILES="$DOTFILES/zsh"

HISTFILE="$HOME/.zsh/cache/`hostname`.zhistory"
HISTSIZE=130000
SAVEHIST=100000
setopt NO_ALL_EXPORT                                                             # end export all


#### History Settings
setopt HIST_IGNORE_DUPS                                                          # ignore adjacent duplicate command lines in scrollback
setopt NO_HIST_VERIFY                                                            # do not review `sudo !!` before executing
setopt INC_APPEND_HISTORY                                                        # append, not replace, to the history file
setopt EXTENDED_HISTORY                                                          # provide timestamps in history
setopt HIST_EXPIRE_DUPS_FIRST                                                    # remove duplicates first before saving history
setopt HIST_IGNORE_SPACE                                                         # do not save command line if it has a leading space
setopt NO_SHARE_HISTORY                                                          # annoying when different terminals do different tasks

####
# Misc Options
####
setopt EXTENDED_GLOB                                                             # use cool zsh glob features (`ls **/filename`, etc)
setopt LONG_LIST_JOBS                                                            # list jobs in long format

####
# Zshrc.d - most other config
# 00 - 09: functions
# 10 - 19: UI. title, prompt, keybindings, etc
# 20 - 29: Aliases.
# 99     : jokes and deprecated
####
for config in "$ZSH_FILES/rc.d"/* ; do
    # echo "loading $config"
    source "$config"
done

#### Host Settings
# [[ -f "$ZSH_FILES/hosts/`hostname`" ]] && source "$ZSH_FILES/hosts/`hostname`"
# [[ -f "$ZSH_FILES/hosts/`hostname`.zsh" ]] && source "$ZSH_FILES/hosts/`hostname`.zsh"
# [[ -f "$HOME/.zsh.local" ]] && source "$HOME/.zsh.local"
